This tool is a wizard to help developers have a better permission coverage via the generation of a list of negative test cases as documented [here](https://gitlab.com/gitlab-com/gl-security/engineering/issues/758).
In the long term, it could be used to automatically generate test cases. More information about the vision around this tool is available in this [Secure issue](https://gitlab.com/gitlab-org/gitlab/issues/207089).

It is **not the source of truth** (yet), [Permissions](https://docs.gitlab.com/ee/user/permissions.html) documentation is.
It is also not exhaustive. But we hope that it can already be helpful to catch as soon as possible permission issues.

This tool is still a MVP. Do not hesitate to provide feedback via [issues](https://gitlab.com/gitlab-com/gl-security/permissionwizard/issues), either on inconsistencies or items/features you would like to see included.

The test cases are sorted by priority, the criteria being the likelihood to detect a security issue (based on our experience from H1 reports):
* P1: "required", developer/maintainer should take them into account in specs
* P2: "recommended", if developer/maintainer/QA have enough time to take them into account
* P3: "nice to have", if they can be generated automatically
* P4: "bonus", if they can be generated automatically and are not too long to execute

NB: Those priorities can be considered as maturity levels. There can be test cases in P3 or P4 with a huge security impact, but it is unlikely that they fail if P1 and P2 tests are successful.

First (naive) implementation has been done in javascript to allow serverless deployments.
This will be reconsidered when dealing with automated test case generation.

Code structure (in order of inclusion):
* `features.js` contains the list of supported features (and the corresponding names of the project setting to restrict/disable the feature)
* `user_info.js` contains the list of different possible roles and user states
* `project_info.js` contains the list of different possible project types
* `group.js` contains the objects representing groups and projects, including the business logic of inheritance and sharing
* `expectations.js` is the object storing the expected results of a test case
* `test_case.js` is the object storing a test case
* `predicates.js` contains the permission rule definitions (implementation of https://docs.gitlab.com/ee/user/permissions.html)
* `priority_helper.js` contains the business logic to decide the priority of a test case
* `generator.js` is the engine generating all the test cases. For a given feature, it iterates over all the possible project access levels, roles and user states.
* `permission_wizard.js` and `permissions_wizard.html` implement a very basic web UI displaying the test cases
* `group_simualtor.js` and `group_simulator.html` implement a live IDE to play with group sharing and inheritance + generate the corresponding test cases

Some features are simple (e.g. wiki) in the sense that do not need to combine test cases from different features.
Some other features are composite: they either
  - concatenate, i.e. OR, simple test cases:  e.g comments
  - intersect, i.e. AND, simple test cases: e.g. milestones (this is currently not handled completely)

In order to add a simple feature
* add it in `features.js` (including project setting names)
* update `roleCanAccess(Read|Write)Feature` methods in `predicates.js` to describe the access conditions of this feature

To add a composite feature, see how `FEATURES.comment` is implemented in `generator.js`.

To be implemented:
* group sharing authorized only with current user groups (requires to add user ownership to sharing implementation)
* group sharing to handle public projects and LDAP group sync
* group inheritance/sharing for public projects
* taking into account group inheritance/sharing in addition to project/user settings
* case when access has just been removed
* system notes
* handle better AND composition (cf `FEATURES.milestone` in `generator.js`)
* create vs update vs delete objects and notion of item owner (e.g for confidential issues)
* archived project cases (read only)
* follow up https://gitlab.com/gitlab-org/gitlab/issues/198558
* anything on https://docs.gitlab.com/ee/user/permissions.html, and particularly the CI/CD permissions
* (XSS protections)
