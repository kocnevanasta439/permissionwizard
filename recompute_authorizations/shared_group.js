"use strict";

function SharedGroup(group, maxRole) {
  this.group = group;
  this.maxRole = maxRole;
}

module.exports = {SharedGroup};
