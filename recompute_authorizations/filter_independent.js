const csv = require('csv-parser');
const fs = require('fs');

const dataFolder = process.env.CSV_FOLDER + "/";
const SPLIT_RATIO = process.env.SPLIT_RATIO;

var namespaceIds = new Set(); //user namespaces that can contain projects, but not linked to a group
var groupIds = new Set();
var projectIds = new Set();

const projectHeader = "id,namespace_id,creator_id";
const memberHeader = "id,user_id,source_id,type,source_type,access_level,expires_at,ldap";
const authorizationHeader = "user_id,project_id,access_level";
const namespaceHeader = "id,path,owner_id,type,parent_id";

var userIds = new Set();
var dependentProjects = [];
var dependentMembers = [];
var splittedAuthorizations = [];

var independentNamespaces = openCSVFile('independent_namespaces.csv', namespaceHeader);
var dependentGroups = openCSVFile('dependent_groups.csv', namespaceHeader);
var dependentGroupMembers = openCSVFile('dependent_group_members.csv', memberHeader);

for(var i=0; i < SPLIT_RATIO; i++) {
  dependentProjects[i] = openCSVFile('dependent_projects_' + i + '.csv', projectHeader);
  dependentMembers[i] = openCSVFile('dependent_members_' + i + '.csv', memberHeader);
  splittedAuthorizations[i] = openCSVFile('splitted_authorizations_' + i + '.csv', authorizationHeader);
}

var invalid_groups_missing_parent = openCSVFile('invalid_groups_missing_parent.csv', namespaceHeader);
var invalid_projects_missing_parent = openCSVFile('invalid_projects_missing_parent.csv', projectHeader);
var invalid_members_missing_user = openCSVFile('invalid_members_missing_user.csv', memberHeader);
var invalid_members_missing_project = openCSVFile('invalid_members_missing_project.csv', memberHeader);
var invalid_members_missing_group = openCSVFile('invalid_members_missing_group.csv', memberHeader);
var invalid_project_authorizations_missing_user = openCSVFile('invalid_project_authorizations_missing_user.csv', authorizationHeader);
var invalid_project_authorizations_missing_project = openCSVFile('invalid_project_authorizations_missing_project.csv', authorizationHeader);

readUsers();

function readUsers() {
  readCSVandProcess('users.csv', processUser, readGroups);
}

function readGroups() {
  readCSVandProcess('sorted_namespaces.csv', processGroup, readProjects);
}

function readProjects() {
  readCSVandProcess('projects.csv', processProject, readMembers);
}

function readMembers() {
  readCSVandProcess('members.csv', processMember, splitProjectAuthorizations);
}

function splitProjectAuthorizations() {
  if (fs.existsSync(dataFolder + 'project_authorizations.csv')) {
    readCSVandProcess('project_authorizations.csv', splitProjectAuthorization, finish)
  } else {
    finish();
  }
}


function finish() {
  dependentGroupMembers.end();
  dependentGroups.end();
  independentNamespaces.end();

  for(var i=0; i < SPLIT_RATIO; i++) {
    dependentProjects[i].end();
    dependentMembers[i].end();
    splittedAuthorizations[i].end();
  }

  invalid_groups_missing_parent.end();
  invalid_projects_missing_parent.end();
  invalid_members_missing_user.end();
  invalid_members_missing_project.end();
  invalid_members_missing_group.end();
  invalid_project_authorizations_missing_user.end();
  invalid_project_authorizations_missing_project.end();
}

function split_function(project_id) {
  return project_id % SPLIT_RATIO;
}

function processUser(candidate) {
  userIds.add(Number(candidate.id));
}

function isEmpty(candidate) {
  if (candidate.id > 0) {
    return false;
  } else {
    return true;
  }
}

function processGroup(candidate) {
  if(isEmpty(candidate)) {
    return;
  }

  var candidateId = Number(candidate.id);
  var result = "\n" + candidate.id + "," + candidate.path + "," + candidate.owner_id + "," +
              candidate.type + "," + candidate.parent_id;

  if ((candidate.parent_id > 0) && !groupIds.has(Number(candidate.parent_id))) {
    invalid_groups_missing_parent.write(result);
    return;
  }
    //TODO MEDIUM check if owner_id exists

  if(candidate.type == "Group") {
    groupIds.add(candidateId); //needed later on for error handling
    dependentGroups.write(result);
  } else {
    namespaceIds.add(candidateId);
    independentNamespaces.write(result);
  }
}

function processProject(candidate) {
  if(isEmpty(candidate)) {
    return;
  }

  var parentId = Number(candidate.namespace_id);
  var candidateId = Number(candidate.id);
  var result = "\n" + candidate.id + "," + candidate.namespace_id + "," + candidate.creator_id;

  if(!groupIds.has(parentId) && !namespaceIds.has(parentId)) {
    invalid_projects_missing_parent.write(result);
    return;
  }
  //TODO MEDIUM check if creator_id exists

  projectIds.add(candidateId);
  var index = split_function(candidateId);
  dependentProjects[index].write(result);
}

function processMember(candidate) {
  if(isEmpty(candidate)) {
    return;
  }

  var user_id = Number(candidate.user_id );
  var project_or_group_id = Number(candidate.source_id);
  var result = "\n" + candidate.id + "," + candidate.user_id + "," + candidate.source_id + "," +
                candidate.type + "," + candidate.source_type + "," + candidate.access_level + "," +
                candidate.expires_at + "," + candidate.ldap;

  if(!userIds.has(user_id)) {
    invalid_members_missing_user.write(result);
    return;
  }

  if ((candidate.type == "ProjectMember") && (candidate.source_type == "Project") ) {
    if(!projectIds.has(project_or_group_id)) {
      invalid_members_missing_project.write(result);
      return;
    }
    //project shared with a group is the same than being a project in a group:
    //groups are stored in common
    var index = split_function(project_or_group_id);
    dependentMembers[index].write(result);
    return;
  }

  //this should now be a group member
  //TODO LOW crosscheck   if ((candidate.type == "GroupMember") && (candidate.source_type == "Namespace")) to be sure
  if(groupIds.has(project_or_group_id)) {
    dependentGroupMembers.write(result);
  } else {
    invalid_members_missing_group.write(result);
  }
}

function splitProjectAuthorization(candidate) {
  var user_id = Number(candidate.user_id);
  var project_id = Number(candidate.project_id);
  var result = "\n" + candidate.user_id + "," + candidate.project_id + "," + candidate.access_level;

  if(!userIds.has(user_id)) {
    invalid_project_authorizations_missing_user.write(result);
    return;
  }
  if(!projectIds.has(project_id)) {
    invalid_project_authorizations_missing_project.write(result);
    return;
  }

  var index = split_function(project_id);
  splittedAuthorizations[index].write(result);
}

function readCSVandProcess(filename, processor, callback) {
  //assumption results are already sorted or order does not matter
  var results =[];
  var startDate = new Date();
  console.log("Starting reading and processing " + filename + " at " + startDate.toGMTString());
  fs.createReadStream(dataFolder + filename)
    .pipe(csv({ separator: ',' }))
    .on('data', (row) => {
      processor(row);
    })
    .on('end', () => {
      var duration = (Date.now() - startDate) / 1000;
      console.log("Finished processing " + filename + " in " + duration + " seconds");
      callback();
    });

}

function openCSVFile(filename, header) {
  fs.writeFile(dataFolder+filename, '', function(){})
  var result = fs.createWriteStream(dataFolder+filename, {
    flags: 'a'
  })
  result.write(header);
  return result;
}
