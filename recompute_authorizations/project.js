const memberMod = require('./member_light');
const sharedgroup = require('./shared_group');

function Project(parent, id) {
  this.parent = parent;
  this.id = id;
  this.members = [];
  this.sharedWith = [];

  this.addMember = function(memberId, role) {
    //check if we dont have this member as inherited, in this case overwrite permission only if they grant more privileges
    var inheritedRole = this.parent.computeRole(memberId);
    if(role > inheritedRole) {
      var member = new memberMod.MemberLight(memberId, true, role);
      this.members.push(member);
    }
  }

  this.removeMember = function(memberId) {
    for(m in this.members) {
      if(this.members[m].id == memberId) {
        this.members.splice(m, 1);
        return;
      }
    }
  }

  this.computeProjectAuthorizations = function(computedProjectAuthorizations) {
    return processProjectAuthorizations(this.listMembers(), this.id, computedProjectAuthorizations);
  }

  this.shareWithGroup = function(group, maxRole) {
    //TODO implement access control
    var sharedGroup = new sharedgroup.SharedGroup(group, maxRole);
    this.sharedWith.push(sharedGroup);
  }

  this.unshareWithGroup = function(group) {
    for(s in this.sharedWith) {
      if(this.sharedWith[s].group == group) {
        this.sharedWith.splice(s, 1);
        return;
      }
    }
  }

  this.listMembers = function() {
    //TODO MEDIUM this could be done in parallel
    var inheritedCopied = [];
    var inheritedMembers = this.parent.listMembers(false);
    for(m in inheritedMembers) {
      var tempMember = new memberMod.MemberLight(inheritedMembers[m].id, false, inheritedMembers[m].role);
      inheritedCopied.push(tempMember); //deep copy needed not to alter original object
    }
    var results = this.members.concat(inheritedCopied);

    var sharedMembers = [];
    for(s in this.sharedWith) {
      var tempMembers = this.sharedWith[s].group.listMembers(false);
      for(t in tempMembers) { //compute effective role
        var effectiveRole = Math.min(tempMembers[t].role, this.sharedWith[s].maxRole);
        var tempMember = new memberMod.MemberLight(tempMembers[t].id, tempMembers[t].direct, effectiveRole);
        sharedMembers.push(tempMember);
      }
    }
    return results.concat(sharedMembers);
  }
}

function processProjectAuthorizations(members, projectId, computedProjectAuthorizations) {

  for(m in members) {
    var name = projectId + "/" +  members[m].id;
    var accessLevel = members[m].role;
    if(computedProjectAuthorizations.has(name)) {
      var currentAccess = computedProjectAuthorizations.get(name);
      if(accessLevel > currentAccess) {
        computedProjectAuthorizations.set(name, accessLevel);
      }
    }
    else {
      computedProjectAuthorizations.set(name, accessLevel);
    }
  }
}

module.exports = {Project};
