const accessLevels = [];
accessLevels[50] = "Owner";
accessLevels[40] = "Maintainer";
accessLevels[30] = "Developer";
accessLevels[20] = "Reporter";
accessLevels[10] = "Guest";
accessLevels[5] = "NotAMember"; //overloaded behavior
accessLevels[0] = "Anonymous";//overloaded behavior

function getRoleName(accessLevel) {
  return accessLevels[accessLevel];
}

module.exports = {getRoleName};
