#export CSV_FOLDER="./test"
export CSV_FOLDER="/Users/jmatos/Desktop/csv_staging"
export RAM=8192
export SPLIT_RATIO=5

node --max-old-space-size=$RAM filter_independent.js

#TODO VERY HIGH do in parallel (but be careful with RAM)
for (( p=0; p<$SPLIT_RATIO; p++ ))
do
  node --max-old-space-size=$RAM recompute.js $p
done
