const memberMod = require('./member_light');
const sharedgroup = require('./shared_group');
const projectMod = require('./project');

const notAMember = 5; //cf roles
const TOP = "toplevel";

function Group(parent, id) {
  this.parent = parent;
  this.id = id;
  this.projects = [];
  this.subgroups = [];
  this.members = [];
  this.sharedWith = [];

  this.computeProjectAuthorizations = function(computedProjectAuthorizations) {
    //we dont need the group members to compute the project authorizations, only the project members
    for(p in this.projects) {
      var proj = this.projects[p];
      proj.computeProjectAuthorizations(computedProjectAuthorizations);
    }

    for(s in this.subgroups) {
      var subgroup = this.subgroups[s];
      subgroup.computeProjectAuthorizations(computedProjectAuthorizations);
    }
  }

  this.addMember = function(memberId, role) {
      //check if we dont have this member as inherited, in this case overwrite permission only if they grant more privileges
      var inheritedRole = this.computeRole(memberId);
      if(role > inheritedRole) {
        var member = new memberMod.MemberLight(memberId, true, role);
        this.members.push(member);
      }
  }

  this.removeMember = function(memberId) {
    for(m in this.members) {
      if(this.members[m].name == memberId) {
        this.members.splice(m, 1);
        return;
      }
    }
  }

  this.listMembers = function(recursiveSharing = true) {
      var indirectCopied = [];
      if(this.parent != TOP) { //if there is a real parent group
        var indirectMembers = this.parent.listMembers(false); //compute indirect members
        for(m in indirectMembers) {
          var tempMember = new memberMod.MemberLight(indirectMembers[m].id, false, indirectMembers[m].role);
          indirectCopied.push(tempMember); //deep copy needed not to alter original object
        }
      }
      var results = this.members.concat(indirectCopied); //add indirect members to direct members

      var sharedMembers = [];
      if(recursiveSharing) {
        for(s in this.sharedWith) {
          var found = findSharedMembers(this.sharedWith[s]);
          sharedMembers = sharedMembers.concat(found);
        }
      }
      results = results.concat(sharedMembers);

      return results;
  }

  this.newProject = function(projectId) {
      var proj = new projectMod.Project(this, projectId);
      this.projects.push(proj);
      return proj;
  }

  this.newSubgroup = function(subgroupId) {
      var subgroup = new Group(this, subgroupId);
      this.subgroups.push(subgroup);
      return subgroup;
  }

  this.computeRole = function(memberId) {
      return findRole(memberId, this.listMembers());
  }

  this.shareWithGroup = function(group, maxRole) {
    //TODO implement access control
    var sharedGroup = new sharedgroup.SharedGroup(group, maxRole);
    this.sharedWith.push(sharedGroup);
  }

  this.unshareWithGroup = function(group) {
    for(s in this.sharedWith) {
      if(this.sharedWith[s].group == group) {
        this.sharedWith.splice(s, 1);
        return;
      }
    }
  }
}

function findRole(memberId, allMembers) {
  for(m in allMembers) {
    var member = allMembers[m];
    if(member.id == memberId) {
      return member.role;
    }
  }
  return notAMember;
}

function findSharedMembers(sharedGroup) {
  var results = [];
  var tempMembers = sharedGroup.group.listMembers(false); //this should not contain recursive shared groups
  for(t in tempMembers) { //compute effective role
    if(tempMembers[t].direct) { //and reject inherited members (feature not yet available in group sharing)
      var effectiveRole = Math.min(tempMembers[t].role, sharedGroup.maxRole);
      var tempMember2 = new memberMod.MemberLight(tempMembers[t].id, true, effectiveRole);
      results.push(tempMember2);
    }
  }
  return results;
}

module.exports = {Group};
