const PROJECT_LEVELS = {
  public: 'Public', publicDisabled: 'PublicDisabled', publicRestricted: 'PublicRestricted',
  internal: 'Internal', private: 'Private'
}

function provideDetailedProjectInfo(feature, projectLevel) {
  if(projectLevel == PROJECT_LEVELS.publicRestricted) {
      return "Public with " + PUBLIC_RESTRICTED_SETTINGS[feature];
  }
  if(projectLevel == PROJECT_LEVELS.publicDisabled) {
      return "Public with " + PUBLIC_DISABLED_SETTINGS[feature];
  }
  return projectLevel;
}
