const ACCESS_STATUS = {
  forbidden: 'Forbidden', na: 'NA', allowed: 'Allowed',
}

//assumption: GraphQL expectations are the same than API
function Expectations(ui_read, ui_write, api_read, api_write) {
  this.uiRead = ui_read;
  this.uiWrite = ui_write;
  this.apiRead = api_read;
  this.apiWrite = api_write;

  this.allNA = function() {
    return (this.uiRead == ACCESS_STATUS.na) &&  (this.uiWrite == ACCESS_STATUS.na) &&
      (this.apiRead == ACCESS_STATUS.na) && (this.apiWrite == ACCESS_STATUS.na);
  }

  this.allallowed = function() {
    return (this.uiRead == ACCESS_STATUS.allowed) &&  (this.uiWrite == ACCESS_STATUS.allowed) &&
      (this.apiRead == ACCESS_STATUS.allowed) && (this.apiWrite == ACCESS_STATUS.allowed);
  }

  this.noforbidden = function() {
    return (this.uiRead != ACCESS_STATUS.forbidden) &&  (this.uiWrite != ACCESS_STATUS.forbidden) &&
      (this.apiRead != ACCESS_STATUS.forbidden) && (this.apiWrite != ACCESS_STATUS.forbidden);
  }

  this.same = function(expected) {
    return (this.uiRead == expected.uiRead) && (this.uiWrite == expected.uiWrite) &&
      (this.apiRead == expected.apiRead) && (this.apiWrite == expected.apiWrite);
  }
}
