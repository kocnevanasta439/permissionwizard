function initDisplay() {
  displayLive(editor.getValue());
  document.getElementById("results").innerHTML = generateTestCases();
}

const TABLE_HEADERS = "<table class=\"table table-bordered table-striped\"><thead class=\"thead-dark\"><tr>" +
  "<th style='width: 5%'>ID</th>" +
  "<th style='width: 10%'>Item sharing (A)</th>" +
  "<th style='width: 10%'>Item invited (B)</th>" +
  "<th style='width: 10%'>*UserB role</th>" +
  "<th style='width: 10%'>Max access level</th>" +
  "<th style='width: 15%'>UserB role in ProjectA</th>" +
  "<th style='width: 15%'>ParentUserB role in ProjectA</th>" +
  "<th style='width: 15%'>ChildUserB role in ProjectA</th>" +
  "</tr></thead>\n";
const FOOTERS = "</table>\n";

var id_counter = 0;

function displayLive(config) {
  eval(config); //this is highly insecure and clearly not XSS protected
  document.getElementById("liveResults").innerHTML = root.display(0);
}

//TODO see how to adapt those test from liveResults than hardcoded root settings
function generateTestCases() {
  var root = new Group(TOP, "root");

  var parentGroupA = root.newSubgroup("ParentGroupA");
  parentGroupA.addMember("OwnerA", ROLES.owner);
  parentGroupA.addMember("UserA", ROLES.reporter);
  var groupA = parentGroupA.newSubgroup("GroupA");
  var childGroupA = groupA.newSubgroup("ChildGroupA");

  var projectA = childGroupA.newProject("ProjectA");
  projectA.addMember("UserA", ROLES.maintainer);

  var parentGroupB = root.newSubgroup("ParentUserB");
  parentGroupB.addMember("OwnerB", ROLES.owner);
  parentGroupB.addMember("ParentUserB", ROLES.guest);
  var groupB = parentGroupB.newSubgroup("GroupB");
  groupB.addMember("UserB", ROLES.guest);
  var childGroupB = groupB.newSubgroup("ChildGroupB");
  childGroupB.addMember("ChildUserB", ROLES.guest);

  var display = root.display(0) + "<br>";
  var maxRole = ROLES.guest;

  var tableExpectations = "";
  id_counter = 0;
  for(var r in ROLES) {
    if(ROLES[r] > ROLES.notAMember) { //only keep significant roles
      //update UserB, ParentUserB and ChildUserB role in their original group
      parentGroupB.removeMember("ParentUserB");
      parentGroupB.addMember("ParentUserB", ROLES[r]);
      groupB.removeMember("UserB");
      groupB.addMember("UserB", ROLES[r]);
      childGroupB.removeMember("ChildUserB");
      childGroupB.addMember("ChildUserB", ROLES[r]);

      tableExpectations += computeTestCases(parentGroupA, parentGroupB, ROLES[r], projectA);
      tableExpectations += computeTestCases(parentGroupA, groupB, ROLES[r], projectA);
      tableExpectations += computeTestCases(parentGroupA, childGroupB, ROLES[r], projectA);

      tableExpectations += computeTestCases(parentGroupA, parentGroupB, ROLES[r], projectA);
      tableExpectations += computeTestCases(parentGroupA, groupB, ROLES[r], projectA);
      tableExpectations += computeTestCases(parentGroupA, childGroupB, ROLES[r], projectA);

      tableExpectations += computeTestCases(childGroupA, parentGroupB, ROLES[r], projectA);
      tableExpectations += computeTestCases(childGroupA, groupB, ROLES[r], projectA);
      tableExpectations += computeTestCases(childGroupA, childGroupB, ROLES[r], projectA);

      tableExpectations += computeTestCases(projectA, parentGroupB, ROLES[r], projectA);
      tableExpectations += computeTestCases(projectA, groupB, ROLES[r], projectA);
      tableExpectations += computeTestCases(projectA, childGroupB, ROLES[r], projectA);
    }
  }

  return display + TABLE_HEADERS + tableExpectations + FOOTERS;
}

function testCase(itemSharing, itemInvited, maxRole, user1, role1, user2, role2, user3, role3) {
  var result = "<br>" + itemSharing.name + " invites " + itemInvited.name + " as maximum " + ROLE_NAMES[maxRole] +": ";

  itemSharing.shareWithGroup(itemInvited, maxRole);
  result += expect(user1, Math.min(maxRole,role1), itemSharing.listMembers());
  result += expect(user2, Math.min(maxRole,role2), itemSharing.listMembers());
  result += expect(user3, Math.min(maxRole,role3), itemSharing.listMembers());

  itemSharing.unshareWithGroup(itemInvited);
  result += expect(user1, ROLES.notAMember, itemSharing.listMembers());
  result += expect(user2, ROLES.notAMember, itemSharing.listMembers());
  result += expect(user3, ROLES.notAMember, itemSharing.listMembers());

  return result;
}

function expect(username, expectedRole, allMembers) {
  var role = findRole(username, allMembers);
  if(role == expectedRole) {
    return "OK,";
  }
  return "Expected role " + expectedRole + " but effective role is " + role + ",";
}

function computeTestCases(itemSharing, itemInvited, roleUserB, targetItem) {
  var result = "<tbody>";

  for(var maxRole in ROLES) {
    if(ROLES[maxRole] > ROLES.notAMember) { //only keep significant roles
      itemSharing.shareWithGroup(itemInvited, ROLES[maxRole]);
      var allMembers = targetItem.listMembers();
      id_counter += 1;

      result += "<tr><td>" + id_counter + "</td><td>" + itemSharing.name + "</td><td>" + itemInvited.name + "</td><td>" +
                ROLE_NAMES[roleUserB] + "</td><td>" + ROLE_NAMES[ROLES[maxRole]] + "</td><td>" +
                ROLE_NAMES[findRole("UserB", allMembers)] + "</td><td>" + ROLE_NAMES[findRole("ParentUserB", allMembers)] + "</td><td>" +
                ROLE_NAMES[findRole("ChildUserB", allMembers)] + "</td></tr>";

      itemSharing.unshareWithGroup(itemInvited);
    }
  }

  return result + "<tr><td></td></tr></tbody>";
}

// Deprecated, could be turned into regression tests: it crosschecks we get the same results than in
//https://docs.google.com/spreadsheets/d/1akP_DRCmrIEh0FsBGbeYqqJJQo3Vp0PmIMYYqr92DbU/edit?usp=sharing
function oldSpreadsheetExpectations() {
  var root = new Group(TOP, "root");

  var parentGroupA = root.newSubgroup("ParentGroupA");
  parentGroupA.addMember("OwnerA", ROLES.owner);
  parentGroupA.addMember("UserA", ROLES.reporter);
  var groupA = parentGroupA.newSubgroup("GroupA");
  var childGroupA = groupA.newSubgroup("ChildGroupA");

  var projectA = childGroupA.newProject("ProjectA");
  projectA.addMember("UserA", ROLES.maintainer);

  var parentGroupB = root.newSubgroup("ParentUserB");
  parentGroupB.addMember("OwnerB", ROLES.owner);
  parentGroupB.addMember("ParentUserB", ROLES.guest);
  var groupB = parentGroupB.newSubgroup("GroupB");
  groupB.addMember("UserB", ROLES.guest);
  var childGroupB = groupB.newSubgroup("ChildGroupB");
  childGroupB.addMember("ChildUserB", ROLES.guest);

  var display = root.display(0);
  var maxRole = ROLES.guest;

  display += "<br><b>Crosschecking spreadsheet expectations</b><br>";
  display += testCase(parentGroupA, parentGroupB, maxRole, "UserB", ROLES.notAMember, "ParentUserB", ROLES.guest, "ChildUserB", ROLES.notAMember);
  display += testCase(parentGroupA, groupB, maxRole, "UserB", ROLES.guest, "ParentUserB", ROLES.notAMember, "ChildUserB", ROLES.notAMember);
  display += testCase(parentGroupA, childGroupB, maxRole, "UserB", ROLES.notAMember, "ParentUserB", ROLES.notAMember, "ChildUserB", ROLES.guest);

  display += "<br>";
  display += testCase(groupA, parentGroupB, maxRole, "UserB", ROLES.notAMember, "ParentUserB", ROLES.guest, "ChildUserB", ROLES.notAMember);
  display += testCase(groupA, groupB, maxRole, "UserB", ROLES.guest, "ParentUserB", ROLES.notAMember, "ChildUserB", ROLES.notAMember);
  display += testCase(groupA, childGroupB, maxRole, "UserB", ROLES.notAMember, "ParentUserB", ROLES.notAMember, "ChildUserB", ROLES.guest);

  display += "<br>";
  display += testCase(childGroupA, parentGroupB, maxRole, "UserB", ROLES.notAMember, "ParentUserB", ROLES.guest, "ChildUserB", ROLES.notAMember);
  display += testCase(childGroupA, groupB, maxRole, "UserB", ROLES.guest, "ParentUserB", ROLES.notAMember, "ChildUserB", ROLES.notAMember);
  display += testCase(childGroupA, childGroupB, maxRole, "UserB", ROLES.notAMember, "ParentUserB", ROLES.notAMember, "ChildUserB", ROLES.guest);

  display += "<br>";
  display += testCase(projectA, parentGroupB, maxRole, "UserB", ROLES.notAMember, "ParentUserB", ROLES.guest, "ChildUserB", ROLES.notAMember);
  display += testCase(projectA, groupB, maxRole, "UserB", ROLES.guest, "ParentUserB", ROLES.guest, "ChildUserB", ROLES.notAMember);
  display += testCase(projectA, childGroupB, maxRole, "UserB", ROLES.guest, "ParentUserB", ROLES.guest, "ChildUserB", ROLES.guest);

  return display;
}
