const TOP = "toplevel";

function SharedGroup(group, maxRole) {
  this.group = group;
  this.maxRole = maxRole;
}

function Project(parent, name) {
  this.parent = parent;
  this.name = name;
  this.members = [];
  this.sharedWith = [];

  this.addMember = function(memberName, role) {
    //check if we dont have this member as inherited, in this case overwrite permission only if they grant more privileges
    var inheritedRole = this.parent.computeRole(memberName);
    if(role > inheritedRole) {
      var member = new Member(memberName, true, role);
      this.members.push(member);
    } else {
      console.log(" Impossible to provide a lower role to "+memberName+ ", current role is "+inheritedRole);
    }
  }

  this.removeMember = function(memberName) {
    for(m in this.members) {
      if(this.members[m].name == memberName) {
        this.members.splice(m, 1);
        return;
      }
    }
  }

  this.display = function(identation) {
    var prefix = "-".repeat(identation);
    if(identation == 1) { prefix = "<br>" + prefix};
    return prefix + "<b>" + this.name + "</b> Members are: " + displayMembers(this.listMembers()) + "<br>";
  }

  this.shareWithGroup = function(group, maxRole) {
    //TODO implement access control
    var sharedGroup = new SharedGroup(group, maxRole);
    this.sharedWith.push(sharedGroup);
  }

  this.unshareWithGroup = function(group) {
    for(s in this.sharedWith) {
      if(this.sharedWith[s].group == group) {
        this.sharedWith.splice(s, 1);
        return;
      }
    }
  }

  this.listMembers = function() {
    var inheritedCopied = [];
    var inheritedMembers = this.parent.listMembers();
    for(m in inheritedMembers) {
      var tempMember = new Member(inheritedMembers[m].name, false, inheritedMembers[m].userInformation.role);
      inheritedCopied.push(tempMember); //deep copy needed not to alter original object
    }
    var results = this.members.concat(inheritedCopied);

    var sharedMembers = [];
    for(s in this.sharedWith) {
      var tempMembers = this.sharedWith[s].group.listMembers();
      for(t in tempMembers) { //compute effective role
        var effectiveRole = Math.min(tempMembers[t].userInformation.role, this.sharedWith[s].maxRole);
        var tempMember = new Member(tempMembers[t].name, tempMembers[t].direct, effectiveRole);
        sharedMembers.push(tempMember);
      }
    }
    return results.concat(sharedMembers);
  }
}

function Group(parent, name) {
  this.parent = parent;
  this.name = name;
  this.projects = [];
  this.subgroups = [];
  this.members = [];
  this.sharedWith = [];

  this.display = function(identation) {
    var prefix = "-".repeat(identation);
    if(identation == 1) { prefix = "<br>" + prefix};
    var results = prefix + "<b>" + this.name + "</b> Members are: " + displayMembers(this.listMembers()) + "<br>";
    if(identation == 0) { //root namespace
      results = "";
    }

    for(p in this.projects) {
      var project = this.projects[p];
      results += project.display(identation+1);
    }

    for(s in this.subgroups) {
      var subgroup = this.subgroups[s];
      results += subgroup.display(identation+1);
    }
    return results;
  }

  this.addMember = function(memberName, role) {
      //check if we dont have this member as inherited, in this case overwrite permission only if they grant more privileges
      var inheritedRole = this.computeRole(memberName);
      if(role > inheritedRole) {
        var member = new Member(memberName, true, role);
        this.members.push(member);
      } else {
        console.log("Impossible to provide a lower role to "+memberName);
      }
  }

  this.removeMember = function(memberName) {
    for(m in this.members) {
      if(this.members[m].name == memberName) {
        this.members.splice(m, 1);
        return;
      }
    }
  }

  this.listMembers = function() {
      var indirectCopied = [];
      if(this.parent != TOP) { //if there is a real parent group
        var indirectMembers = this.parent.listMembers(); //compute indirect members
        for(m in indirectMembers) {
          var tempMember = new Member(indirectMembers[m].name, false, indirectMembers[m].userInformation.role);
          indirectCopied.push(tempMember); //deep copy needed not to alter original object
        }
      }
      var results = this.members.concat(indirectCopied); //add indirect members to direct members

      var sharedMembers = [];
      for(s in this.sharedWith) {
        var found = findSharedMembers(this.sharedWith[s]);
        sharedMembers = sharedMembers.concat(found);
      }
      results = results.concat(sharedMembers);

      return results;
  }

  this.newProject = function(projectName) {
      var project = new Project(this, projectName);
      this.projects.push(project);
      return project;
  }

  this.newSubgroup = function(subgroupName) {
      var subgroup = new Group(this, subgroupName);
      this.subgroups.push(subgroup);
      return subgroup;
  }

  this.computeRole = function(memberName) {
      return findRole(memberName, this.listMembers());
  }

  this.shareWithGroup = function(group, maxRole) {
    //TODO implement access control
    var sharedGroup = new SharedGroup(group, maxRole);
    this.sharedWith.push(sharedGroup);
  }

  this.unshareWithGroup = function(group) {
    for(s in this.sharedWith) {
      if(this.sharedWith[s].group == group) {
        this.sharedWith.splice(s, 1);
        return;
      }
    }
  }
}

function findSharedMembers(sharedGroup) {
  var results = [];
  var tempMembers = sharedGroup.group.listMembers();
  for(t in tempMembers) { //compute effective role
    if(tempMembers[t].direct) { //and reject inherited members (feature not yet available in group sharing)
      var effectiveRole = Math.min(tempMembers[t].userInformation.role, sharedGroup.maxRole);
      var tempMember2 = new Member(tempMembers[t].name, true, effectiveRole);
      results.push(tempMember2);
    }
  }
  return results;
}

function findRole(memberName, allMembers) {
  for(m in allMembers) {
    var member = allMembers[m];
    if(member.name == memberName) {
      return member.userInformation.role;
    }
  }
  return ROLES.notAMember;
}
