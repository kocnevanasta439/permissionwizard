//supported features
const FEATURES = {issue:0, mr:1, milestone:2, code:3, commit:4, wiki:5, snippet:6, comment:7, confidential_issue: 8, issues:9};
const FEATURE_NAMES = ["Issue", "Merge request", "Milestone", "Code", "Commit", "Wiki", "Snippet", "Comment", "Confidential issue", "All issues"];

const PUBLIC_RESTRICTED_SETTINGS = [
  "Issues as Project Members Only",
  "Merge Requests as Project Members Only",
  "Issues and Merge Requests as Project Members Only",
  "Repository as Project Members Only",
  "Repository as Project Members Only",
  "Wiki as Project Members Only",
  "Snippets as Project Members Only",
  "useless",
  "Issues as Project Members Only",
  "useless"
]

const PUBLIC_DISABLED_SETTINGS = [
  "Issues disabled",
  "Merge Requests disabled",
  "Issues and Merge Requests disabled",
  "Repository disabled",
  "Repository disabled",
  "Wiki disabled",
  "Snippets disabled",
  "useless",
  "Issues disabled",
  "useless"
]
