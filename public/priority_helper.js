
function decidePriorityFromProjectAccessLevel(accessLevel) {
  return (accessLevel == PROJECT_LEVELS.publicDisabled) ? 2 : 1;
}

function decidePriorityForNonActivatedUser(userState) {
    return (userState != USER_STATES.activated) ? 2 : 0;
}

function decidePriorityForExternalUser(projectLevel) {
  if (projectLevel == PROJECT_LEVELS.internal || projectLevel == PROJECT_LEVELS.private || projectLevel == PROJECT_LEVELS.publicRestricted ) {
    return 1;
  }  else if (projectLevel == PROJECT_LEVELS.publicDisabled) {
    return 2;
  } else { //full public project
    return 4;
  }
}

function decidePriorityFromLowerRole(testCase) {
  var feature = testCase.feature;
  var userInfo = testCase.userInformation;
  if(userInfo.role == 0) {
    return 0;
  }

  var lowerUserInfo = new UserInformation(userInfo.role-1, userInfo.activationState, userInfo.external, userInfo.auditor, userInfo.admin);
  var lowerExpected = computeExpectations(feature, userInfo.role-1, testCase.projectInformation, userInfo.activationState, userInfo.external);
  if (testCase.sameThanLowerRole(lowerUserInfo, lowerExpected)) {
    if(testCase.expectations.allallowed()) {
      return 4; //lowest priority, everything is already authorized with a lower role
    } else {
      return 1;
    }
  }

  return 0;
}

function deprioritizeUselessTestCases(testCases) {
  //for role independant criteria: find out test cases were all scenarios are NA
  for(var t in testCases) {
    if (testCases[t].expectations.allNA() || testCases[t].priority > 4) {
        testCases[t].priority = 4;
    }
  }
}
